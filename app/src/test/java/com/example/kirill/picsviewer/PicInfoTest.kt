package com.example.kirill.picsviewer

import org.junit.Assert
import org.junit.Test

class PicInfoTest {
    @Test
    fun equality() {
        val picInfo1 = PicInfo("id", "url1", PicInfo.Publisher("1", "2", "3", "4"))
        val picInfo2 = PicInfo("id", "url2", PicInfo.Publisher("d", "2", "g", "4"))
        //comparing ids only
        Assert.assertEquals(picInfo1, picInfo2)
    }
}