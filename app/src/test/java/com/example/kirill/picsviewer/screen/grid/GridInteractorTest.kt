package com.example.kirill.picsviewer.screen.grid

import com.example.kirill.picsviewer.TestCoroutineWrapperFactory
import com.example.kirill.picsviewer.api.Gif
import com.example.kirill.picsviewer.api.GiphyServiceProvider
import com.example.kirill.picsviewer.api.GiphyServiceWrapper
import com.example.kirill.picsviewer.api.Image
import com.example.kirill.picsviewer.api.Images
import com.example.kirill.picsviewer.api.Pagination
import com.example.kirill.picsviewer.api.TrendingResponse
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyInt
import org.mockito.Mockito.eq
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.TimeoutException

class GridInteractorTest {

  @Mock
  lateinit var giphyService: GiphyServiceWrapper
  @Mock
  lateinit var serviceProvider: GiphyServiceProvider

  @Before
  fun before() {
    MockitoAnnotations.initMocks(this)
    `when`(serviceProvider.giphyServiceWrapper).thenReturn(giphyService)
  }

  @Suppress("UNCHECKED_CAST")
  @Test
  fun refresh() {
    val response = TrendingResponse(
        listOf(Gif(
            "id",
            "gif",
            Images(Image("http://link.to")),
            null)),
        Pagination(1, 1, 0)
    )

    `when`(giphyService.trending(anyInt(), anyInt())).thenReturn(response)

    val interactor = GridInteractor(serviceProvider, TestCoroutineWrapperFactory())
    val countDownLatch = CountDownLatch(3)

    interactor.onProgressStatusChangeListener = { _ ->
      countDownLatch.countDown()
    }

    interactor.refresh(1)

    if (!countDownLatch.await(500, MILLISECONDS)) {
      throw TimeoutException()
    }

    verify(giphyService).trending(eq(0), anyInt())
    Assert.assertFalse(interactor.isProgressing)
  }
}