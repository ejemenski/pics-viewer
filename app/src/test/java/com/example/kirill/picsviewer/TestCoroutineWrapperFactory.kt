package com.example.kirill.picsviewer

import com.example.kirill.picsviewer.util.CoroutineWrapper
import kotlinx.coroutines.experimental.Dispatchers

class TestCoroutineWrapperFactory : CoroutineWrapper.Factory {
  override fun create(errorHandler: (Throwable) -> Unit): CoroutineWrapper {
    return CoroutineWrapper(Dispatchers.Default, errorHandler)
  }
}