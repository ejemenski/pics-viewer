package com.example.kirill.picsviewer.api

import android.support.annotation.WorkerThread
import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection.HTTP_OK

open class GiphyServiceWrapper(private val giphyService: GiphyService) {
  @WorkerThread
  open fun trending(offset: Int, limit: Int): TrendingResponse? {
    return giphyService.trending(offset, limit, API_KEY).execute().checkResponse().body()
  }

  @WorkerThread
  open fun findById(id: String): FindByIdResponse? {
    return giphyService.findById(id, API_KEY).execute().checkResponse().body()
  }

  private fun <T> Response<T>.checkResponse(): Response<T> {
    if (code() != HTTP_OK) {
      throw HttpException(this)
    }
    return this
  }
}