package com.example.kirill.picsviewer

interface Mvp {

  interface View

  abstract class Presenter<V : View>(protected val view: V) {
    abstract fun startPresenting()
    abstract fun stopPresenting()
  }
}