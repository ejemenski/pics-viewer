package com.example.kirill.picsviewer.screen.loader

import android.support.annotation.MainThread
import com.example.kirill.picsviewer.util.CoroutineWrapper
import com.example.kirill.picsviewer.util.ImageLoader
import com.example.kirill.picsviewer.PicInfo
import com.example.kirill.picsviewer.api.GiphyServiceProvider
import com.example.kirill.picsviewer.util.toPicInfo

class LoaderInteractor(
    serviceProvider: GiphyServiceProvider,
    private val coroutineWrapperFactory: CoroutineWrapper.Factory, private val imageLoader: ImageLoader
) {

    private val giphyService = serviceProvider.giphyServiceWrapper
    var result: PicInfo? = null
        private set
        @MainThread get
    var error: Throwable? = null
        private set
        @MainThread get
    private var isProgressing = false

    var onErrorListener: ((Throwable) -> Unit)? = null
        @MainThread set(value) {
            field = value
            error?.let { value?.invoke(it) }
        }
    var onResultListener: ((PicInfo) -> Unit)? = null
        @MainThread set(value) {
            field = value
            result?.let { value?.invoke(it) }
        }

    fun load(id: String) : Boolean {
        if (isProgressing) {
            return false
        }
        isProgressing = true
        coroutineWrapperFactory.create { e ->
            error = e
            isProgressing = false
            onErrorListener?.invoke(e)
        }.async {
            val response = awaitIO { giphyService.findById(id) }
            if (response == null) {
                onErrorListener?.invoke(IllegalStateException("empty response"))
                return@async
            }

            val picInfo = response.gif.toPicInfo()
            awaitIO { imageLoader.preloadSync(picInfo.url) }
            result = picInfo
            isProgressing = false
            onResultListener?.invoke(picInfo)
        }
        return true
    }
}
