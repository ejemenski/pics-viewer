package com.example.kirill.picsviewer.screen.loader

import com.example.kirill.picsviewer.Mvp
import com.example.kirill.picsviewer.PicInfo

interface LoaderMvpView : Mvp.View {
  fun openPic(picInfo: PicInfo)
  fun showError(e: Throwable)
  fun showProgress()
}