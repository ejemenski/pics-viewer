package com.example.kirill.picsviewer.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class GiphyServiceProvider {

  open val giphyServiceWrapper: GiphyServiceWrapper by lazy { GiphyServiceWrapper(makeGiphyService()) }

  private fun makeGiphyService(): GiphyService {
    val retrofit = Retrofit.Builder()
        .baseUrl(Urls.BASE_API_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    return retrofit.create(GiphyService::class.java)
  }
}