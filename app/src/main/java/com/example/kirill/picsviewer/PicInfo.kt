package com.example.kirill.picsviewer

import android.os.Parcel
import android.os.Parcelable

class PicInfo(val id: String, val url: String, val publisher: Publisher?) : Parcelable {

  constructor(parcel: Parcel) : this(
      parcel.readString(),
      parcel.readString(),
      parcel.readParcelable(Publisher::class.java.classLoader)
  )

  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(id)
    parcel.writeString(url)
    parcel.writeParcelable(publisher, flags)
  }

  override fun describeContents(): Int {
    return 0
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as PicInfo

    if (id != other.id) return false

    return true
  }

  override fun hashCode(): Int {
    return id.hashCode()
  }

  companion object CREATOR : Parcelable.Creator<PicInfo> {
    override fun createFromParcel(parcel: Parcel): PicInfo {
      return PicInfo(parcel)
    }

    override fun newArray(size: Int): Array<PicInfo?> {
      return arrayOfNulls(size)
    }
  }

  class Publisher(val username: String, val displayName: String?, val twitter: String?,
      val userProfileUrl: String) :
      Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
      parcel.writeString(username)
      parcel.writeString(displayName)
      parcel.writeString(twitter)
      parcel.writeString(userProfileUrl)
    }

    override fun describeContents(): Int {
      return 0
    }

    companion object CREATOR : Parcelable.Creator<Publisher> {
      override fun createFromParcel(parcel: Parcel): Publisher {
        return Publisher(parcel)
      }

      override fun newArray(size: Int): Array<Publisher?> {
        return arrayOfNulls(size)
      }
    }
  }
}