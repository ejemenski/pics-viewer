package com.example.kirill.picsviewer.screen.grid

import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Slide
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kirill.picsviewer.util.CoroutineWrapper
import com.example.kirill.picsviewer.R
import com.example.kirill.picsviewer.api.GiphyServiceProvider

class GridFragment : Fragment() {

  private val interactor by lazy {
    GridInteractor(GiphyServiceProvider(), CoroutineWrapper.Android)
  }

  private var presenter: GridPresenter? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    retainInstance = true
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? {
    return layoutInflater.inflate(R.layout.fragment_grid, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    presenter = GridPresenter(GridViewHolder(view as ViewGroup, fragmentManager!!), interactor)
    presenter!!.startPresenting()
  }

  override fun onDestroyView() {
    super.onDestroyView()
    presenter!!.stopPresenting()
    presenter = null
  }

  companion object {
    fun newInstance() : GridFragment {
      val f = GridFragment()
      f.exitTransition = Slide()
      f.enterTransition = Slide()
      return f
    }
  }
}