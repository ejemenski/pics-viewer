package com.example.kirill.picsviewer

import android.content.Intent

class IntentHandler(private val callback: Callback) {

    fun handleIntent(intent: Intent) {

        if (intent.action != Intent.ACTION_VIEW) {
            callback.openGrid()
            return
        }

        try {
            val id = with(intent.data) {
                when {
                    (scheme == "http" || scheme == "https") && host == "giphy.com" && path.startsWith(
                        "/gifs/"
                    ) ->
                        pathSegments.last()
                    scheme == "picsviewer" && host == "gifs" && path.isNotBlank() -> pathSegments.last()
                    else -> throw WrongIntent()
                }
            }
            callback.openGif(id)
        } catch (e: WrongIntent) {
            callback.onWrongIntent()
        }
    }

    interface Callback {
        fun openGif(id: String)
        fun onWrongIntent()
        fun openGrid()
    }

    class WrongIntent : Exception()
}