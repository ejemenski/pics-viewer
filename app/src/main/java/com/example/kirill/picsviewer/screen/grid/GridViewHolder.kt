package com.example.kirill.picsviewer.screen.grid

import android.support.v4.app.FragmentManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.example.kirill.picsviewer.util.ImageLoader
import com.example.kirill.picsviewer.R
import com.example.kirill.picsviewer.screen.detailed.DetailedFragment
import kotlinx.android.synthetic.main.fragment_grid.view.recyclerView

class GridViewHolder(
    container: ViewGroup,
    private val fragmentManager: FragmentManager
) : GridMvpView {


  private val recyclerView = container.recyclerView
  private val adapter = RecyclerViewAdapterInner()
  private val swipeRefreshLayout = container as SwipeRefreshLayout
  private val context = container.context
  private val imageLoader = ImageLoader(context)

  init {
    recyclerView.layoutManager = GridLayoutManager(context, 3)
    recyclerView.adapter = adapter
    swipeRefreshLayout.setOnRefreshListener { callback?.onPullToRefresh() }
  }

  override var callback: GridMvpView.Callback? = null

  override fun setItems(items: List<GridMvpView.Item>) {
    adapter.items = items
  }


  override fun setProgressing(isProgressing: Boolean) {
    swipeRefreshLayout.isRefreshing = isProgressing
  }

  override fun showError(e: Throwable) {
    Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
  }

  override fun openPic(index: Int) {

    val item = adapter.items[index] as? GridMvpView.PicItem ?: return

    fragmentManager.beginTransaction()
        .replace(R.id.container, DetailedFragment.newInstance(item.picInfo))
        .addToBackStack(null)
        .commit()
  }

  inner class RecyclerViewAdapterInner : RecyclerView.Adapter<ViewHolder>() {

    var items: List<GridMvpView.Item> = emptyList()
      set(value) {
        field = value
        notifyDataSetChanged()
      }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

      val layoutId = when (viewType) {
        TYPE_PIC -> R.layout.item
        TYPE_NEXT -> R.layout.next
        else -> throw IllegalStateException()
      }

      val itemView = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)

      val h = parent.resources.displayMetrics.let { Math.min(it.heightPixels, it.widthPixels) / 4 }

      itemView.layoutParams.height = h

      return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = when (items[position]) {
      is GridMvpView.PicItem -> TYPE_PIC
      is GridMvpView.NextItem -> TYPE_NEXT
      else -> throw IllegalStateException()
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      val item = items[position]
      when (item) {
        is GridMvpView.PicItem -> {
          val pic = item.picInfo
          imageLoader.loadImage(pic.url, holder.itemView as ImageView)
          holder.itemView.setOnClickListener {
            callback?.onItemSelect(position)
          }
        }
        is GridMvpView.NextItem -> {
          callback?.onNext()
        }
      }
    }
  }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

  companion object {
    const val TYPE_PIC = 0
    const val TYPE_NEXT = 1
  }
}