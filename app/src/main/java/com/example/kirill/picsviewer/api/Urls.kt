package com.example.kirill.picsviewer.api

object Urls {
  const val BASE_API_URL = "https://api.giphy.com/v1/gifs/"
  private const val BASE_MEDIA_URL = "https://media.giphy.com/media/"

  fun fixMediaUrl(url: String): String {
    if (url.startsWith("http://") || url.startsWith("https://")) {
      return url
    }
    return "$BASE_MEDIA_URL$url"
  }
}