package com.example.kirill.picsviewer

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.kirill.picsviewer.screen.grid.GridFragment
import com.example.kirill.picsviewer.screen.loader.LoaderFragment


class MainActivity : AppCompatActivity(), IntentHandler.Callback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState != null) {
            return
        }

        IntentHandler(this).handleIntent(intent)
    }

    override fun openGif(id: String) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, LoaderFragment.newInstance(id))
            .commit()
    }

    override fun openGrid() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.container, GridFragment.newInstance())
            .commit()

    }

    override fun onWrongIntent() {
        Log.e("MainActivity", "wrong intent")
        finish()
    }
}
