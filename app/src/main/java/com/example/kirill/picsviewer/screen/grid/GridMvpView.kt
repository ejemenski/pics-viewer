package com.example.kirill.picsviewer.screen.grid

import com.example.kirill.picsviewer.Mvp
import com.example.kirill.picsviewer.PicInfo

interface GridMvpView : Mvp.View {

  var callback: Callback?
  fun setItems(items: List<Item>)
  fun setProgressing(isProgressing: Boolean)
  fun showError(e: Throwable)
  fun openPic(index: Int)

  interface Callback {
    fun onItemSelect(index: Int)
    fun onPullToRefresh()
    fun onNext()
  }

  interface Item

  class PicItem(val picInfo: PicInfo) : Item

  class NextItem : Item
}