package com.example.kirill.picsviewer.util

import android.content.Context
import android.support.annotation.WorkerThread
import android.widget.ImageView
import com.example.kirill.picsviewer.R.drawable
import com.example.kirill.picsviewer.glide.GlideApp

class ImageLoader(context: Context) {
  private val requests = GlideApp.with(context)

  fun loadImage(url: String, view: ImageView) {
    requests.load(url).error(drawable.ic_error_24dp).fitCenter().into(view)
  }

  @WorkerThread
  fun preloadSync(url: String) {
    requests.load(url).submit().get()
  }
}