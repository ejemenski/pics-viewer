package com.example.kirill.picsviewer.screen.grid

import android.support.annotation.MainThread
import com.example.kirill.picsviewer.util.CoroutineWrapper
import com.example.kirill.picsviewer.PicInfo
import com.example.kirill.picsviewer.api.Gif
import com.example.kirill.picsviewer.api.GiphyServiceProvider
import com.example.kirill.picsviewer.util.toPicInfo
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.properties.Delegates

class GridInteractor(serviceProvider: GiphyServiceProvider,
    private val coroutineWrapperFactory: CoroutineWrapper.Factory) {

  private val giphyService = serviceProvider.giphyServiceWrapper
  private val trending = CopyOnWriteArrayList<PicInfo>()

  var hasNext = false
    @MainThread get
    private set

  val hasData
    @MainThread get() = !trending.isEmpty()

  var isProgressing by Delegates.observable(false) { _, _, new ->
    onProgressStatusChangeListener?.invoke(new)
  }
    @MainThread get
    private set

  var onChangeListener: ((Collection<PicInfo>) -> Unit)? = null
    @MainThread set(value) {
      field = value
      value?.invoke(trending)
    }
  var onErrorListener: ((Throwable) -> Unit)? = null
    @MainThread set

  var onProgressStatusChangeListener: ((Boolean) -> Unit)? = null
    @MainThread set(value) {
      field = value
      value?.invoke(isProgressing)
    }

  @MainThread
  fun refresh(count: Int): Boolean {
    return requestTrending(0, count, true)
  }

  @MainThread
  fun next(count: Int): Boolean {
    return requestTrending(trending.size, count, false)
  }

  private fun requestTrending(offset: Int, count: Int, clearBefore: Boolean): Boolean {
    if (isProgressing) {
      return false
    }
    isProgressing = true
    if (clearBefore) {
      trending.clear()
    }
    coroutineWrapperFactory.create { e ->
      onErrorListener?.invoke(e)
      isProgressing = false
    }.async {
      val response = awaitIO { giphyService.trending(offset, count) }
      hasNext = if (response != null) {
        val pics = await { response.data.map(Gif::toPicInfo) }
        await { trending.addAllAbsent(pics) }
        with(response.pagination) { offset + count < totalCount }
      } else {
        false
      }
      onChangeListener?.invoke(trending)
      isProgressing = false
    }
    return true
  }
}