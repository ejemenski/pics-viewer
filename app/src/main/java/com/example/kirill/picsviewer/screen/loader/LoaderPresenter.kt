package com.example.kirill.picsviewer.screen.loader

import com.example.kirill.picsviewer.Mvp

class LoaderPresenter(
    view: LoaderMvpView, private val interactor: LoaderInteractor,
    private val id: String
) : Mvp.Presenter<LoaderMvpView>(view) {

    override fun startPresenting() {

        if (interactor.error == null  && interactor.result == null) {
            interactor.load(id)
            view.showProgress()
        }

        interactor.onResultListener = view::openPic
        interactor.onErrorListener = view::showError
    }

    override fun stopPresenting() {
        interactor.onResultListener = null
        interactor.onErrorListener = null
    }
}