package com.example.kirill.picsviewer.screen.detailed

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.text.util.LinkifyCompat
import android.text.util.Linkify
import android.transition.Slide
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.example.kirill.picsviewer.util.ImageLoader
import com.example.kirill.picsviewer.PicInfo
import com.example.kirill.picsviewer.R
import kotlinx.android.synthetic.main.fragment_detailed.displayNameView
import kotlinx.android.synthetic.main.fragment_detailed.picView
import kotlinx.android.synthetic.main.fragment_detailed.profileUrlView
import kotlinx.android.synthetic.main.fragment_detailed.twitterTagView
import kotlinx.android.synthetic.main.fragment_detailed.usernameView

class DetailedFragment : Fragment() {

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_detailed, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    val picInfo: PicInfo = arguments?.getParcelable(PIC_ARG) ?: return

    val imageLoader = ImageLoader(view.context)
    imageLoader.loadImage(picInfo.url, picView)

    with(picInfo.publisher) {
      if (this == null) {
        usernameView.setText(R.string.no_data)
        return
      }
      usernameView.text = username
      displayNameView.text = displayName
      displayNameView.visibility = if (displayName.isNullOrBlank()) GONE else VISIBLE
      twitterTagView.text = twitter
      twitterTagView.visibility = if (twitter.isNullOrBlank()) GONE else VISIBLE
      profileUrlView.text = userProfileUrl
    }
    LinkifyCompat.addLinks(profileUrlView, Linkify.WEB_URLS)
  }

  companion object {
    private const val PIC_ARG = "PIC_ARG"
    fun newInstance(picInfo: PicInfo): DetailedFragment {
      val f = DetailedFragment()
      f.enterTransition = Slide()
      f.exitTransition = Slide()
      val args = Bundle()
      args.putParcelable(PIC_ARG, picInfo)
      f.arguments = args
      return f
    }
  }
}