package com.example.kirill.picsviewer.screen.loader

import android.support.v4.app.FragmentManager
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.example.kirill.picsviewer.PicInfo
import com.example.kirill.picsviewer.R
import com.example.kirill.picsviewer.screen.detailed.DetailedFragment
import kotlinx.android.synthetic.main.fragment_loader.view.errorView
import kotlinx.android.synthetic.main.fragment_loader.view.progressBar

class LoaderViewHolder(private val container: View, private val fragmentManager: FragmentManager) : LoaderMvpView {

    override fun openPic(picInfo: PicInfo) {
        container.progressBar.visibility = GONE
        fragmentManager.beginTransaction()
            .replace(R.id.container, DetailedFragment.newInstance(picInfo))
            .commit()
    }

    override fun showError(e: Throwable) {
        container.progressBar.visibility = GONE
        container.errorView.text = e.message
    }

    override fun showProgress() {
        container.progressBar.visibility = VISIBLE
        container.errorView.text = null
    }
}