package com.example.kirill.picsviewer.util

import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.CoroutineExceptionHandler
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.CoroutineStart
import kotlinx.coroutines.experimental.Dispatchers
import kotlinx.coroutines.experimental.Dispatchers.Main
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch

open class CoroutineWrapper(dispatcher: CoroutineDispatcher,
    private val errorHandler: (Throwable) -> Unit) {

  private val scope = CoroutineScope(dispatcher + CoroutineExceptionHandler { context, e ->
    dispatcher.dispatch(context, kotlinx.coroutines.experimental.Runnable {
      errorHandler.invoke(e)
    })
  })

  open fun async(block: suspend CoroutineWrapper.() -> Unit) {
    scope.launch {
      block.invoke(this@CoroutineWrapper)
    }
  }

  open suspend fun <T> await(block: suspend CoroutineScope.() -> T): T {
    return scope.async(Dispatchers.Default, CoroutineStart.DEFAULT, block).await()
  }

  open suspend fun <T> awaitIO(block: suspend CoroutineScope.() -> T): T {
    return scope.async(Dispatchers.IO, CoroutineStart.DEFAULT, block).await()
  }

  interface Factory {
    fun create(errorHandler: (Throwable) -> Unit) : CoroutineWrapper
  }

  object Android : Factory {
    override fun create(errorHandler: (Throwable) -> Unit): CoroutineWrapper {
      return CoroutineWrapper(Main, errorHandler)
    }
  }
}