package com.example.kirill.picsviewer.screen.grid

import android.util.Log
import com.example.kirill.picsviewer.Mvp.Presenter

class GridPresenter(view: GridMvpView,
    private val gridInteractor: GridInteractor) : Presenter<GridMvpView>(view), GridMvpView.Callback {

  override fun startPresenting() {
    if (!gridInteractor.hasData && !gridInteractor.isProgressing) {
      gridInteractor.refresh(ITEM_COUNT)
    }
    gridInteractor.onChangeListener = { pics ->
      var items = pics.map { GridMvpView.PicItem(it) as GridMvpView.Item }
      if (gridInteractor.hasNext) {
        items += GridMvpView.NextItem()
      }
      view.setItems(items)
    }
    gridInteractor.onProgressStatusChangeListener = view::setProgressing
    gridInteractor.onErrorListener = view::showError
    view.callback = this
  }

  override fun stopPresenting() {
    gridInteractor.onChangeListener = null
    gridInteractor.onProgressStatusChangeListener = null
    gridInteractor.onErrorListener = null
    view.callback = null
  }

  override fun onItemSelect(index: Int) {
    view.openPic(index)
  }

  override fun onPullToRefresh() {
    if (!gridInteractor.refresh(ITEM_COUNT)) {
      logBusy()
    }
  }

  override fun onNext() {
    if (!gridInteractor.next(ITEM_COUNT)) {
      logBusy()
    }
  }

  private fun logBusy() {
    Log.d("GridPresenter", "interactor is busy")
  }

  companion object {
    private const val ITEM_COUNT = 48
  }
}