package com.example.kirill.picsviewer.api

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("username") val username: String,
    @SerializedName("display_name") val displayName: String?,
    @SerializedName("twitter") val twitter: String?,
    @SerializedName("profile_url") val profileUrl: String
)


data class Image(
    @SerializedName("url") val url: String
)

data class Images(
    @SerializedName("fixed_width") val fixedWidth: Image
)

data class Gif(
    @SerializedName("id") val id: String,
    @SerializedName("title") val title: String,
    @SerializedName("images") val images: Images,
    @SerializedName("user") val user: User?
)

data class Pagination(
    @SerializedName("total_count") val totalCount: Int,
    @SerializedName("count") val count: Int,
    @SerializedName("offset") val offset: Int
)

data class TrendingResponse(
    @SerializedName("data") val data: List<Gif>,
    @SerializedName("pagination") val pagination: Pagination
)

data class FindByIdResponse(
    @SerializedName("data") val gif: Gif
)
