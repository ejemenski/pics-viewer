package com.example.kirill.picsviewer.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GiphyService {
  @GET("trending")
  fun trending(
      @Query("offset") offset: Int,
      @Query("limit") limit: Int,
      @Query("api_key") apiKey: String
  ): Call<TrendingResponse>

  @GET("{id}")
  fun findById(@Path("id") id: String, @Query("api_key") apiKey: String): Call<FindByIdResponse>
}