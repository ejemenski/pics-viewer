package com.example.kirill.picsviewer.util

import com.example.kirill.picsviewer.PicInfo
import com.example.kirill.picsviewer.PicInfo.Publisher
import com.example.kirill.picsviewer.api.Gif
import com.example.kirill.picsviewer.api.Urls

fun Gif.toPicInfo() = PicInfo(
    id,
    Urls.fixMediaUrl(images.fixedWidth.url),
    if (user != null) Publisher(user.username,
        user.displayName, user.twitter,
        user.profileUrl) else null
)