package com.example.kirill.picsviewer.screen.loader

import android.os.Bundle
import android.support.v4.app.Fragment
import android.transition.Slide
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.kirill.picsviewer.util.CoroutineWrapper
import com.example.kirill.picsviewer.util.ImageLoader
import com.example.kirill.picsviewer.R
import com.example.kirill.picsviewer.api.GiphyServiceProvider

class LoaderFragment : Fragment() {

  private val interactor by lazy {
    LoaderInteractor(GiphyServiceProvider(), CoroutineWrapper.Android,
        ImageLoader(context!!))
  }
  private var presenter: LoaderPresenter? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    retainInstance = true
  }

  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
      savedInstanceState: Bundle?): View? {
    return inflater.inflate(R.layout.fragment_loader, container, false)
  }

  override fun onResume() {
    super.onResume()
    presenter = LoaderPresenter(LoaderViewHolder(view!!, fragmentManager!!), interactor,
        arguments!!.getString(ID_ARG))
    presenter!!.startPresenting()
  }

  override fun onPause() {
    super.onPause()
    presenter!!.stopPresenting()
    presenter = null
  }

  companion object {
    private const val ID_ARG = "ID_ARG"
    fun newInstance(id: String): LoaderFragment {
      val args = Bundle()
      args.putString(ID_ARG, id)
      val f = LoaderFragment()
      f.arguments = args
      f.enterTransition = Slide()
      f.exitTransition = Slide()
      return f
    }
  }
}
