package com.example.kirill.picsviewer

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import org.junit.Test

class OpenGridTest: ActivityTestRule<MainActivity>(MainActivity::class.java) {

  @Test fun openGrid()  {
    launchActivity(null)
    onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
  }
}